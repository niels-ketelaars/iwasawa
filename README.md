p-Adic L-functions
==========

A SageMath module for computing Iwasawa power series of p-adic L-functions over Q.


Author
------

Niels Ketelaars, <https://nielsk.nl>


Prerequisites
-------------

- SageMath, <https://www.sagemath.org/>. The module is confirmed to work with version 9.7, but probably works on older versions too.


Examples
-------------

We can compute the `5`-adic L function attached to the square of the Teichmuller character.

    sage: p = 5
    sage: L = pAdicL(5)
    sage: L.series(2)
    (2 + O(5)) + (1 + O(5))*T + O(5)*T^2 + (1 + O(5))*T^3 + (1 + O(5))*T^4 + O(T^5)

We can increase the number of terms, and the `p`-adic accuracy of each term
(beware that increasing the accuracy becomes computationally inefficient
very quickly):

    sage: L.series(2, 10, 4)
    (2 + 5 + 2*5^2 + 4*5^3 + O(5^4)) + (1 + 4*5 + 4*5^2 + 2*5^3 + O(5^4))*T + (3*5 + 5^2 + 4*5^3 + O(5^4))*T^2 + (1 + 2*5 + 2*5^2 + 3*5^3 + O(5^4))*T^3 + (1 + 3*5 + 4*5^2 + 5^3 + O(5^4))*T^4 + (4 + 2*5^3 + O(5^4))*T^5 + (3*5 + 2*5^2 + 3*5^3 + O(5^4))*T^6 + (1 + 2*5 + 5^2 + 4*5^3 + O(5^4))*T^7 + (4 + 4*5^2 + O(5^4))*T^8 + (1 + 5 + 5^2 + 4*5^3 + O(5^4))*T^9 + O(T^10)

We can also compute the series for the trivial character (which will have a `T^{-1}` term):

    sage: L.series(0, n=4)
    (1 + 5 + 2*5^2 + O(5^3))*T^-1 + (3 + 5 + 5^2 + O(5^3)) + (4 + 4*5 + 4*5^2 + O(5^3))*T + (4 + 4*5 + 2*5^2 + O(5^3))*T^2 + (2 + 3*5 + O(5^3))*T^3 + O(T^4)

The prime `p=37` is the smallest for which one of the series has a zero,
which can be seen as the first term is divisible by `p`:

    sage: L = pAdicL(37)
    sage: L.series(32, n=3)
    (30*37 + O(37^2)) + (21 + 30*37 + O(37^2))*T + (8 + 6*37 + O(37^2))*T^2 + (35 + 6*37 + O(37^2))*T^3 + (15 + 4*37 + O(37^2))*T^4 + O(T^5)

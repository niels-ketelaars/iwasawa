from sage.rings.integer_ring import ZZ
from sage.rings.rational_field import QQ
from sage.rings.padics.factory import Qp
from sage.rings.cc import CC
from sage.rings.all import LaurentSeriesRing, PowerSeriesRing, Integers

from sage.rings.integer import Integer
from sage.arith.all import valuation, binomial
from sage.arith.misc import primitive_root

from sage.structure.sage_object import SageObject
from sage.interfaces.gp import gp

import numpy as np


def printProgressBar(
    iteration,
    total,
    prefix="",
    suffix="",
    decimals=1,
    length=100,
    fill="█",
    printEnd="\r",
):
    """
    Call in a loop to create terminal progress bar
    @params:
        iteration   - Required  : current iteration (Int)
        total       - Required  : total iterations (Int)
        prefix      - Optional  : prefix string (Str)
        suffix      - Optional  : suffix string (Str)
        decimals    - Optional  : positive number of decimals in percent complete (Int)
        length      - Optional  : character length of bar (Int)
        fill        - Optional  : bar fill character (Str)
        printEnd    - Optional  : end character (e.g. "\r", "\r\n") (Str)
    """
    percent = ("{0:." + str(decimals) + "f}").format(100 * (iteration / float(total)))
    filledLength = int(length * iteration // total)
    bar = fill * filledLength + "-" * (length - filledLength)
    print(f"\r{prefix} |{bar}| {percent}% {suffix}", end=printEnd)
    # Print New Line on Complete
    if iteration == total:
        print()


class pAdicL(SageObject):
    r"""
    A class for computing the Iwasawa power series of `p`-adic L-functions
    over `\QQ`.
    """

    def __init__(self, p, global_prec=20):
        r"""
        INPUT:

        - ``p`` -- a prime number
        """
        self.p = p
        self.a = primitive_root(p**2)
        self.global_prec = global_prec

    def frac_part(self, x):
        r"""
        Compute the `p`-adic fractional part of `x` as a rational number.

        INPUT:

        - ``x`` -- a rational number

        OUTPUT:

        - a rational number, the `p`-adic fractional part of `x`

        EXAMPLES::

            sage: L = pAdicL(5)
            sage: L.frac_part(356/35)
            3/5
        """
        p = self.p
        val = -valuation(x, p)
        x = Qp(p)(x, prec=2 * val) % 1
        return QQ(x.lift())

    def measure_mu(self, r, n):
        r"""
        Return `\mu_a(r + p^n \ZZ_p)`, where `\mu_a` is the measure whose Amice
        transform is

        `\frac{a}{(1+T)^a - 1} - \frac{1}{T}`

        where `a` is a primitive root modulo `p^2`. The result is a rational number.

        INPUT:

        - ``r`` -- a rational number
        - ``n`` -- a positive integer

        OUTPUT:

        - a rational number

        """
        p = QQ(self.p)
        a = QQ(self.a)
        res = (
            a * (self.frac_part(r / (a * p**n)) - QQ(1 / 2))
            - self.frac_part(r / p**n)
            + QQ(1 / 2)
        )
        return QQ(res)

    def measure_la(self, r, n):
        r"""
        Return `\int_{r+p^n \ZZ_p} x^{-1} d\mu_a` where `\mu_a` is the measure whose Amice
        transform is

        `\frac{a}{(1+T)^a - 1} - \frac{1}{T}`

        where `a` is a primitive root modulo `p^2`. The result accurate modulo `p^n`.

        INPUT:

        - ``r`` -- a rational number
        - ``n`` -- a positive integer

        OUTPUT:

        - a rational number
        """
        res = QQ(1 / r) * self.measure_mu(r, n)
        return res

    def measure_dirac(self, r, n):
        r"""
        Return the measure of `r+p^n \ZZ_p` under the measure `[1]-[a]`, where `a`
        is a primitive root modulo `p^2` and [\cdot] is the dirac measure.

        INPUT:

        - ``r`` -- a rational number
        - ``n`` -- a positive integer

        OUTPUT:

        - an integer, either `0`, `1` or `-1`
        """
        p = self.p
        res = 0
        if (r - 1) % (p**n) == 0:
            res = res + 1
        if (r - self.a) % (p**n) == 0:
            res = res - 1
        return res

    def teichmuller(self, prec):
        r"""
        Return Teichmuller lifts to the given precision.

        INPUT:

        - ``prec`` -- a positive integer.

        OUTPUT:

        - a list of integers, the Teichmuller lifts

        EXAMPLES::

            sage: L = pAdicL(7)
            sage: L.teichmuller(1)
            [0, 1, 2, 3, 4, 5, 6]
            sage: L.teichmuller(2)
            [0, 1, 30, 31, 18, 19, 48]
        """
        K = Qp(self.p, prec)
        return [Integer(0)] + [w.residue(prec).lift() for w in K.teichmuller_system()]

    def bound(self, terms=5, n=2):
        r"""
        Return the minimum valuation of the first `terms` coefficients
        of `(1+T)^{p^n}-1`. This is also the accuracy of the approximation
        of the `p`-adic L-function.

        INPUT:

        - ``terms`` -- (default: 5) a positive integer.
        - ``n`` -- (default: 2) a positive integer.

        OUTPUT:

        - a positive integer

        EXAMPLES::

            sage: L = pAdicL(11)
            sage: L.bound()
            2
            sage: L.bound(12, 6)
            5
        """
        pn = self.p**n
        res = [valuation(binomial(pn, j), self.p) for j in range(1, terms)]
        return min(res)

    def series(self, char, terms=5, n=1, *, progress=False):
        r"""
        Return the `n`-th approximation to the `p`-adic L-series, in the
        component corresponding to the `char`-th power of the Teichmuller
        character, as a power series in `T`. Each coefficient is a
        `p`-adic number whose precision is provably correct.

        INPUT:

        - ``char`` -- an integer between 0 and p-2, the power of the Teichmuller character to use
        - ``terms`` -- (default: 5) a positive integer, the number of terms to compute
        - ``n`` -- (default: 2) a positive integer, controls the `p`-adic accuracy
        - ``progress`` -- (default: False) display a progress bar when computing

        EXAMPLES:

        We compute some `p`-adic L-functions::

            sage: p = 5
            sage: L = pAdicL(5)
            sage: L.series(2)
            (2 + O(5)) + (1 + O(5))*T + O(5)*T^2 + (1 + O(5))*T^3 + (1 + O(5))*T^4 + O(T^5)

        We can increase the number of terms, and the `p`-adic accuracy of each term
        (beware that increasing the accuracy becomes computationally infeasible
        very quickly)::

            sage: L.series(2, 10, 4)
            (2 + 5 + 2*5^2 + 4*5^3 + O(5^4)) + (1 + 4*5 + 4*5^2 + 2*5^3 + O(5^4))*T + (3*5 + 5^2 + 4*5^3 + O(5^4))*T^2 + (1 + 2*5 + 2*5^2 + 3*5^3 + O(5^4))*T^3 + (1 + 3*5 + 4*5^2 + 5^3 + O(5^4))*T^4 + (4 + 2*5^3 + O(5^4))*T^5 + (3*5 + 2*5^2 + 3*5^3 + O(5^4))*T^6 + (1 + 2*5 + 5^2 + 4*5^3 + O(5^4))*T^7 + (4 + 4*5^2 + O(5^4))*T^8 + (1 + 5 + 5^2 + 4*5^3 + O(5^4))*T^9 + O(T^10)

        We can also compute the series for the trivial character (which will have a `T^{-1}` term)::

            sage: L.series(0, n=4)
            (1 + 5 + 2*5^2 + O(5^3))*T^-1 + (3 + 5 + 5^2 + O(5^3)) + (4 + 4*5 + 4*5^2 + O(5^3))*T + (4 + 4*5 + 2*5^2 + O(5^3))*T^2 + (2 + 3*5 + O(5^3))*T^3 + O(T^4)

        The prime `p=37` is the smallest for which one of the series has a zero,
        which can be seen as the first term is divisible by `p`::

            sage: L = pAdicL(37)
            sage: L.series(32, n=3)
            (30*37 + O(37^2)) + (21 + 30*37 + O(37^2))*T + (8 + 6*37 + O(37^2))*T^2 + (35 + 6*37 + O(37^2))*T^3 + (15 + 4*37 + O(37^2))*T^4 + O(T^5)
        """
        p = self.p
        a = self.a
        prec = max(n + 2, 20)
        K = Qp(p, prec)
        R = LaurentSeriesRing(QQ, "T", terms + 5)
        T = R.gen().add_bigoh(terms + 5)
        L = R(0)
        one_plus_T_factor = R(1).add_bigoh(terms + 5)

        gamma = QQ(1 + p)
        gamma_power = QQ(1)
        teich = self.teichmuller(prec=n + 1)

        if progress:
            printProgressBar(0, p ** (n), length=50)
        for j in range(p ** (n)):
            s = QQ(0)
            for i in range(1, p):
                r = teich[i] * gamma_power
                s += teich[i] ** char * (self.measure_la(r, n + 1))
            L += s * one_plus_T_factor
            one_plus_T_factor *= 1 + T
            gamma_power *= gamma
            if progress:
                printProgressBar(j + 1, p ** (n), length=50)

        G = R(0)

        gamma = QQ(1 + p)
        gamma_power = QQ(1)
        teicha = QQ(K.teichmuller(a).residue(n + 1).lift())

        gammap = K(1 + p)
        proja = K(a / teicha)

        loga = proja.log() / gammap.log()
        power = loga.residue(n).lift()
        G = 1 - teicha**char * (1 + T) ** (power)
        f = L / G
        bound = self.bound(terms, n)

        R = LaurentSeriesRing(K, "T", terms + 5)
        f = R(f).add_bigoh(terms)
        aj = f.list()
        aj = [aj[j].add_bigoh(bound) for j in range(0, len(aj))]
        f = R(aj).add_bigoh(terms)
        if char % (p - 1) != 0:
            return f
        else:
            return f / R.gen()
